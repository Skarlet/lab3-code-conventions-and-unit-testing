package com.hw.db.controllers;

import com.hw.db.DAO.ThreadDAO;
import com.hw.db.DAO.UserDAO;
import com.hw.db.models.Post;
import com.hw.db.models.Thread;
import com.hw.db.models.User;
import com.hw.db.models.Vote;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.MockedStatic;
import org.mockito.Mockito;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

class threadControllerTests {

    private List<Post> postList;
    private Thread thread;
    private User user;

    @BeforeEach
    void createElements(){
        Post post = new Post("testAuthor", Timestamp.valueOf("2020-02-01 10:00:00.0"), "testForum",
                "testmsg", 1, 2, true);
        postList = new ArrayList<>();
        postList.add(post);
        thread = new Thread();
        user = new User("testAuthor", "user@mail.com" ,"testName", "some");
    }


    @Test
    @DisplayName("Correct CheckIdOrSlug function work")
    void testCheckIdOrSlug(){
        threadController controller = new threadController();
        try (MockedStatic<ThreadDAO> threadMock = Mockito.mockStatic(ThreadDAO.class)) {
            threadMock.when(() -> ThreadDAO.getThreadBySlug("test"))
                    .thenReturn(thread);
            threadMock.when(() -> ThreadDAO.getThreadById(0)).thenReturn(thread);
            assertEquals(thread,
                    controller.CheckIdOrSlug("test"));
            assertEquals(thread, controller.CheckIdOrSlug("0"));

        }
    }

    @Test
    @DisplayName("Correct post creation test")
    void correctlyCreatesPost() {
        try (MockedStatic<ThreadDAO> threadMock = Mockito.mockStatic(ThreadDAO.class)) {
            threadMock.when(() -> ThreadDAO.getThreadBySlug("test"))
                    .thenReturn(thread);
            threadController controller = new threadController();
            try(MockedStatic<UserDAO> userMock = Mockito.mockStatic(UserDAO.class)) {
                userMock.when(() -> UserDAO.Info("testAuthor")).thenReturn(user);

                assertEquals(ResponseEntity.status(HttpStatus.CREATED).body(postList),
                        controller.createPost("test", postList));
            }
        }
    }

    @Test
    @DisplayName("Correct posts display test")
    void testPosts(){
        thread.setId(0);
        threadController controller = new threadController();
        try (MockedStatic<ThreadDAO> threadMock = Mockito.mockStatic(ThreadDAO.class)) {
            threadMock.when(() -> ThreadDAO.getThreadBySlug("test"))
                    .thenReturn(thread);
            threadMock.when(() -> ThreadDAO.getPosts(0, 2,1, "flat", false)).thenReturn(postList);
            assertEquals(ResponseEntity.status(HttpStatus.OK).body(postList),
                        controller.Posts("test", 2, 1, "flat", false));
        }
    }

    @Test
    @DisplayName("Correct change function test")
    void testChange(){
        thread.setId(0);
        Thread threadNew = new Thread();
        threadNew.setId(0);
        threadController controller = new threadController();
        try (MockedStatic<ThreadDAO> threadMock = Mockito.mockStatic(ThreadDAO.class)) {
            threadMock.when(() -> ThreadDAO.getThreadBySlug("test"))
                    .thenReturn(thread);
            threadMock.when(() -> ThreadDAO.change(thread, threadNew)).thenCallRealMethod();
            threadMock.when(() -> ThreadDAO.getThreadById(0)).thenReturn(threadNew);
            assertEquals(ResponseEntity.status(HttpStatus.OK).body(threadNew), controller.change("test", threadNew));
        }

    }

    @Test
    @DisplayName("Correct info function work")
    void testInfo(){
        threadController controller = new threadController();
        try (MockedStatic<ThreadDAO> threadMock = Mockito.mockStatic(ThreadDAO.class)) {
            threadMock.when(() -> ThreadDAO.getThreadBySlug("test"))
                    .thenReturn(thread);
            assertEquals(ResponseEntity.status(HttpStatus.OK).body(thread),
                    controller.info("test"));

        }
    }

    @Test
    @DisplayName("Correct create vote")
    void testCreateVote(){

        final int votes = 5;
        final int voices = 4;

        thread.setVotes(votes);
        Vote vote = mock(Vote.class);
        when(vote.getNickname()).thenReturn("testAuthor");
        when(vote.getVoice()).thenReturn(voices);

        threadController controller = new threadController();
        try (MockedStatic<ThreadDAO> threadMock = Mockito.mockStatic(ThreadDAO.class)) {
            threadMock.when(() -> ThreadDAO.getThreadBySlug("test"))
                    .thenReturn(thread);
            try(MockedStatic<UserDAO> userMock = Mockito.mockStatic(UserDAO.class)) {
                userMock.when(() -> UserDAO.Info("testAuthor")).thenReturn(user);
                ResponseEntity resp = controller.createVote("test", vote);
                assertEquals(ResponseEntity.status(HttpStatus.OK).body(thread), resp);
                assertEquals(voices+votes, thread.getVotes().intValue());
            }
        }
    }
}
